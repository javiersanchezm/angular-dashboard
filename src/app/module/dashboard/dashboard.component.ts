import { Component, Input, OnInit } from '@angular/core';
import { Chart, ChartType, Colors } from 'chart.js/auto';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  vistas: number = 1504;
  sales: number = 80;
  comments: number = 284;
  earning: number = 7842;

  @Input() valueIn!: string;

  datos = [
    {
      name: 'star refrigerator',
      price: 1200,
      payment: 'paid',
      status: 'delivered'
    },
    {
      name: 'dell laptop',
      price: 110,
      payment: 'due',
      status: 'pending'
    },
    {
      name: 'apple watch',
      price: 1200,
      payment: 'paid',
      status: 'return'
    },
    {
      name: 'adidas shoes',
      price: 620,
      payment: 'due',
      status: 'in progress'
    },
    {
      name: 'star refrigerator',
      price: 1200,
      payment: 'paid',
      status: 'delivered'
    },
    {
      name: 'dell laptop',
      price: 110,
      payment: 'due',
      status: 'pending'
    },
    {
      name: 'apple watch',
      price: 1200,
      payment: 'paid',
      status: 'return'
    },
    {
      name: 'adidas shoes',
      price: 620,
      payment: 'due',
      status: 'in progress'
    }
  ];

  recentCustomers = [
    {
      name: 'david',
      city: 'italy',
      url: '../../../assets/images/profile-1.jpg'
    },
    {
      name: 'mario',
      city: 'spain',
      url: '../../../assets/images/profile-2.jpg'
    },
    {
      name: 'maria',
      city: 'brazil',
      url: '../../../assets/images/profile-3.jpg'
    },
    {
      name: 'ana',
      city: 'united states',
      url: '../../../assets/images/profile-4.jpg'
    },
    {
      name: 'david',
      city: 'italy',
      url: '../../../assets/images/profile-1.jpg'
    },
    {
      name: 'mario',
      city: 'spain',
      url: '../../../assets/images/profile-2.jpg'
    },
    {
      name: 'maria',
      city: 'brazil',
      url: '../../../assets/images/profile-3.jpg'
    },
  ];

  
  // public ctx:HTMLElement = document.getElementById('myChart');
  public chart!: Chart;
  public chartPolar!: Chart;

  ngOnInit(): void {

    this.chart = new Chart("chart", {
      type: 'bar' as ChartType,
      data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November' , 'December'],
        datasets: [{
          label: 'Earning',
          data: [1100, 1050, 3000, 5500, 1100, 3500, 4800, 4600, 3500, 8500, 4500, 9000],
          borderWidth: 1,
          backgroundColor: ['rgb(255, 99, 132)','rgb(75, 192, 192)','rgb(255, 205, 86)','rgb(201, 203, 207)','rgb(54, 162, 235)']
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });

    this.chartPolar = new Chart("chartPolar", {
      type: 'polarArea' as ChartType,
      data: {
        labels: ['Facebook','Youtube','Amazon'],
        datasets: [{
          data: [700, 1100, 1600],
          backgroundColor: [
            'rgb(255, 99, 132)',
            'rgb(75, 192, 192)',
            'rgb(255, 205, 86)',
          ]
        }]
      }

    });

  }
  

}
