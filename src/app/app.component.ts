import { ChangeDetectorRef, Component } from '@angular/core';
import { MenuService } from './services/menu/menu.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'Dashboard';
  /* Variable para abrir o cerrar el menú y ajustar los estilos */
  cerrar = false;

  constructor(private menuService: MenuService,
    private changeDetectorRef: ChangeDetectorRef ) { }

  /* Renderice el contenido al cambio de valor de la variable */
  ngAfterViewChecked(): void {
    this.cerrar = this.menuService.obtenerInformacion();
    this.changeDetectorRef.detectChanges();
  }
}
