import { Component } from '@angular/core';
import { MenuService } from 'src/app/services/menu/menu.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  ocultar = true;
  valorInput = '';

  constructor( private menuService: MenuService) { }

  /* Enviar el valor para ocultar o no el sidenav */
  toggle(){
    this.ocultar = !this.ocultar;
    this.menuService.guardarInformacion(this.ocultar)
  }

}
