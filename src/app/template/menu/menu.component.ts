import { ChangeDetectorRef, Component } from '@angular/core';
import { MenuService } from 'src/app/services/menu/menu.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent{
  /* Variable para abrir o cerrar el menú y ajustar los estilos */
  ocultar = false;

  constructor(private menuService: MenuService,
              private changeDetectorRef: ChangeDetectorRef ) { }

  /* Renderice el contenido al cambio de valor de la variable */
  ngAfterViewChecked(): void {
    this.ocultar = this.menuService.obtenerInformacion();
    this.changeDetectorRef.detectChanges();
  }
}
