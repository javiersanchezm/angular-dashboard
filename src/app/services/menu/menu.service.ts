import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  compartirEstado = true;

  guardarInformacion(estado: any) {
    this.compartirEstado = estado;
    console.log("Estado",estado);
  }

  obtenerInformacion() {
    console.log("Compartir estado", this.compartirEstado);
    return this.compartirEstado;
  }
}
