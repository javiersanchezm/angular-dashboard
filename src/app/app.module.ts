import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './template/header/header.component';
import { MenuComponent } from './template/menu/menu.component';
import { CustomerComponent } from './module/customer/customer.component';
import { DashboardComponent } from './module/dashboard/dashboard.component';
import { MessageComponent } from './module/message/message.component';
import { HelpComponent } from './module/help/help.component';
import { SettingsComponent } from './module/settings/settings.component';
import { PasswordComponent } from './module/password/password.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    CustomerComponent,
    DashboardComponent,
    MessageComponent,
    HelpComponent,
    SettingsComponent,
    PasswordComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
