import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerComponent } from './module/customer/customer.component';
import { DashboardComponent } from './module/dashboard/dashboard.component';
import { MessageComponent } from './module/message/message.component';
import { HelpComponent } from './module/help/help.component';
import { SettingsComponent } from './module/settings/settings.component';
import { PasswordComponent } from './module/password/password.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    pathMatch: 'full'
  },
  {
    path: 'customers',
    component: CustomerComponent
  },
  {
    path: 'messages',
    component: MessageComponent
  },
  {
    path: 'help',
    component: HelpComponent
  },
  {
    path: 'settings',
    component: SettingsComponent
  },
  {
    path: 'password',
    component: PasswordComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
